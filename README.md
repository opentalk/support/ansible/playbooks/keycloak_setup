# Manage OpenTalk Keycloak Clients with Ansible within an existing Realm

## Dependencies

Ensure that Ansible is installed following the official documentation. Also, verify that the Ansible host can establish an SSH connection to the target machine using public key authentication.
To run the Ansible playbook, you need the `community.general` collection and the `ot_keycloak_clients` role. You can install both by executing the following command with the provided `requirements.yml` file:

```bash
ansible-galaxy install -r requirements.yml
```

Review the playbook `manage_ot_keycloak_clients.yml` and adjust variables if necessary. You can find the current available variables for role `ot_keycloak_clients` below.

## Run the playbook

```bash
ansible-playbook --connection=local -i localhost, manage_ot_keycloak_clients.yml
```

### !NOTE

- This approach works without an Ansible inventory. Feel free to use your existing one. However, if you run the playbook against a single target or on loclahost, don't forget the comma attached to the hostname. This is because <ip|host>, is processed by the `host_lists` Ansible inventory plugin, which dynamically creates an inventory from a series of targets separated by commas. If there aren't commas in your target specification, the plugin won't trigger and, thus, won't run the playbook.

## Currently availabe variables

| variable                             | type   | default value                                             |descriptiom                                    | mandatory |
|--------------------------------------|--------|-----------------------------------------------------------|-----------------------------------------------|-----------|
| OPENTALK_BASE_DOMAIN                 | string | opentalk.example.com                                      | domain used for the OpenTalk WebUI            | x         |
| AUTH_KEYCLOAK_URL                    | string | https://keycloak.opentalk.example.com/auth                | keycloak URL to authenticate                  | x         |
| AUTH_REALM                           | string | master                                                    | realm to authenticate                         | x         |
| AUTH_USERNAME                        | string | admin                                                     | admin user                                    | x         |
| AUTH_PASSWORD                        | string | SECRET                                                    | admin password                                | x         |
| KEYCLOAK_TARGET_REALM                | string | {{ AUTH_REALM }}                                          | the realm to create the resources in          |           |
|                                      |        |                                                           | AUTH_REALM is used if not defined             |           |
| BACKEND_CLIENT_ID                    | string | OtBackend                                                 | name of the backend client                    |           |
| BACKEND_CLIENT_SECRET                | string | SECRET                                                    | secret of the backend client                  | x         |
| BACKEND_CLIENT_REDIRECT_URIS         | list   | ["https://{{ OPENTALK_BASE_DOMAIN }}"]                    | a list of valid redirect URIs                 |           |
| BACKEND_CLIENT_WEB_ORIGINS           | list   | []                                                        | a list of alowed CORS origins                 |           |
| FRONTEND_CLIENT_ID                   | string | OtFrontend                                                | name of the frontend client                   |           |
| FRONTEND_CLIENT_REDIRECT_URIS        | list   | [                                                         |                                               |           |
|                                      |        |  "https://{{ OPENTALK_BASE_DOMAIN }}/auth/popup_callback",| a list of valid redirect URIs                 |           |
|                                      |        |  "https://{{ OPENTALK_BASE_DOMAIN }}/auth/callback",      |                                               |           |
|                                      |        |  "https://{{ OPENTALK_BASE_DOMAIN }}/dashboard",          |                                               |           |
|                                      |        |  "https://{{ OPENTALK_BASE_DOMAIN }}/"                    |                                               |           |
|                                      |        | ]                                                         |                                               |           |
| FRONTEND_CLIENT_WEB_ORIGINS          | list   |["https://{{ OPENTALK_BASE_DOMAIN }}"]                     | a list of alowed CORS origins                 |           |
| OBELISK_CLIENT_ID                    | string | Obelisk                                                   | name of the obelisk client                    |           |
| OBELISK_CLIENT_SECRET                | string | SECRET                                                    | secret of the obelisk client                  | x         |
| OBELISK_CLIENT_REDIRECT_URIS         | list   | []                                                        | a list of valid redirect URIs                 |           |
| OBELISK_CLIENT_WEB_ORIGINS           | list   | []                                                        | a list of alowed CORS origins                 |           |
| RECORDER_CLIENT_ID                   | string | Recorder                                                  | name of the recorder client                   |           |
| RECORDER_CLIENT_SECRET               | string | SECRET                                                    | secret of the recorder client                 | x         |
| RECORDER_CLIENT_REDIRECT_URIS        | list   | []                                                        | a list of valid redirect URIs                 |           |
| RECORDER_CLIENT_WEB_ORIGINS          | list   | []                                                        | a list of alowed CORS origins                 |           |
| REALM_MANAGEMENT_CLIENT_ID           | string | realm-management                                          | name of the realm-management client           |           |
| REALM_MANAGEMENT_REDIRECT_URIS       | list   | []                                                        | a list of valid redirect URIs                 |           |
| REALM_MANAGEMENT_WEB_ORIGINS         | list   | []                                                        | a list of alowed CORS origins                 |           |
| REALM_MANAGEMENT_ROLES_NO_COMPOSITES | list   | []                                                        | a list of roles to create without composities |           |
| REALM_MANAGEMENT_REALM_ADMIN_ROLES   | list   | []                                                        | a list of roles to assign to realm-admin role |           |
| TESTUSER_CREATE                      | bool   | false                                                     | create and manage the testuser                |           |
| TESTUSER_ENABLE                      | bool   | true                                                      | enable the testuser                           |           |
| TESTUSER_PASSWORD                    | string | SECRET                                                    | set testuser password                         |           |
